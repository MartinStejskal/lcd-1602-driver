/**
 * \file
 *
 * \brief Hardware Abstraction Layer for AVR8 and LCD 1602 module.\n
 * Use generic pins (no hardware module needed)
 *
 * HAL driver for 4bit communication with module LCD 1602.\n
 * Contains basic low level functions which depend on user architecture and\n
 * hardware. Thanks to this layer is possible use common higher level driver\n
 * and apply it thru many architectures and devices. Also this allow easy\n
 * updates independent to higher layer (new features and so on).
 *
 * Created:  25.12.2013\n
 * Modified: 26.12.2013
 *
 * \version 0.1a
 * \author Martin Stejskal
 */

#ifndef _LCD_1602_HAL_AVR8_SW_interface_library_
#define _LCD_1602_HAL_AVR8_SW_interface_library_
///! \brief Just inform, that HAL was included (for preprocessor checks)
#define LCD_1602_HAL_INCLUDED

/* Please define CPU speed F_CPU (#define F_CPU 1000000UL) or include library
 * where F_CPU is defined (#include "some_h_file_with_F_CPU.h"
 */
#define F_CPU   1000000UL

//===========================| Included libraries |============================

#ifndef F_CPU
#error "F_CPU is not defined. Please define F_CPU (frequency of CPU clock) or\
  just include library, where is defined your F_CPU constant. If CPU clock is\
  variable it is highly recommended to set F_CPU to maximum clock."
#endif

#include <avr/io.h>             // Inputs/outputs
#include <inttypes.h>           // Define integer types
#include <avr/pgmspace.h>       /* For work with memory. There is used for save
                                 * text to flash, because these texts are not
                                 * changing.
                                 */

#include <util/delay.h>         // Delays


//==============================| User settings |==============================
/**
 * \brief Backlight support
 *
 * Please set this value to 1 (true) if you want enable support for\n
 * backlight, or 0 (false) if you do not need backlight support. When support\n
 * is enabled LCD_1602_LIGHT_PORT and LCD_1602_LIGHT_PIN must be defined!
 */
#define LCD_1602_backlight_support      0

/**
 * \defgroup Pin_configuration Pin configuration
 *
 * There is possible define which pin is connected to defined LCD signal
 *
 * @{
 */
/**
 * \name Register select
 * @{
 */
// Port name (A, B, C and so on)
#define LCD_1602_RS_PORT     D
// Pin number. Can be assign as PD6, it is same
#define LCD_1602_RS_PIN      6

/**
 * @}
 * \name Enable signal
 * @{
 */
#define LCD_1602_E_PORT      A
#define LCD_1602_E_PIN       0
/**
 * @}
 * \name Data signals
 * @{
 */
#define LCD_1602_D4_PORT     D
#define LCD_1602_D4_PIN      2

#define LCD_1602_D5_PORT     D
#define LCD_1602_D5_PIN      3

#define LCD_1602_D6_PORT     D
#define LCD_1602_D6_PIN      4

#define LCD_1602_D7_PORT     D
#define LCD_1602_D7_PIN      5
/**
 * @}
 * \name Backlight signal
 *
 * Valid only if LCD_1602_backlight_support is enabled. When support is\n
 * disabled, then defined pin will not be used.
 * @{
 */
#define LCD_1602_LIGHT_PORT  D
#define LCD_1602_LIGHT_PIN   0

/**
 * @}
 * @}
 */


//===============================| Definitions |===============================

/**
 * \name Definitions for default LCD settings
 * @{
 */
#define LCD_1602_cursor_off             0
#define LCD_1602_cursor_not_blink       1
#define LCD_1602_cursor_blink           2
/**
 * @}
 */

//============================| Structures, enums |============================

/**
 * \brief Enumeration of states, which can function returns
 *
 * Generally if function return non-zero value then no problem occurs. This\n
 * enum also use LCD_5110 driver (higher layer of this driver).
 */
typedef enum{
  LCD_1602_ERROR = -1,       //!< Unspecified error
  LCD_1602_OK = 0,           //!< All OK
  LCD_1602_ERROR_TIMEOUT = 1,//!< Timeout occurs
  LCD_1602_ERROR_INVALID_PARAMETER = 2  //|< Invalid parameter
} lcd_1602_status_t;


//===========================| Function prototypes |===========================
/**
 * \brief Initialize hardware and clear screen
 * @param i_default_cursor LCD_1602_cursor_off/LCD_1602_cursor_not_blink/\n
 *   LCD_1602_cursor_blink
 * @return LCD_1602_OK (0) if all OK
 */
lcd_1602_status_t LCD_1602_HAL_init(uint8_t i_default_cursor);

/**
 * \brief Delete all symbols on LCD
 *
 * @return LCD_1602_OK (0) if all OK
 */
lcd_1602_status_t LCD1602_HAL_Clear_display(void);

/**
 * \brief Set address on LCD (means cursor position).
 *
 * "First" line has address range from 0x00 to 0x27. The "second" line\n
 * begins at 0x40 thru 0x67.
 *
 * @param i_addr Cursor address
 * @return LCD_1602_OK (0) if all OK
 */
lcd_1602_status_t LCD1602_HAL_set_address( uint8_t i_addr );


/**
 * \brief Write one symbol on LCD
 *
 * Example: LCD1602_write_char('C'); // Write "C"
 *
 * @param i_char Input ASCII character
 */
lcd_1602_status_t LCD1602_HAL_write_char( uint8_t i_char );


/**
 * \brief Turn on backlight on LCD module (set pin to high)
 *
 * When backlight support is disabled, then return LCD_1602_ERROR. Else set\n
 * defined pin (LCD_1602_LIGHT_PORT, LCD_1602_LIGHT_PIN) to high and return\n
 * LCD_1602_OK.
 *
 * \note It is highly recommended use transistor as current booster for\n
 * backlight. Generally it is not recommended connect backlight directly to\n
 * MCU pin.
 *
 * @return LCD_1602_OK (0) if all OK
 */
lcd_1602_status_t LCD1602_HAL_backlight_on(void);

/**
 * \brief Turn off backlight on LCD module (set pin to low)
 *
 * When backlight support is disabled, then return LCD_1602_ERROR. Else set\n
 * defined pin (LCD_1602_LIGHT_PORT, LCD_1602_LIGHT_PIN) to low and return\n
 * LCD_1602_OK.
 *
 * \note It is highly recommended use transistor as current booster for\n
 * backlight. Generally it is not recommended connect backlight directly to\n
 * MCU pin.
 *
 * @return LCD_1602_OK (0) if all OK
 */
lcd_1602_status_t LCD1602_HAL_backlight_off(void);

/**
 * \brief Delay for higher layers
 *
 * Higher layers should use this functions due to differences of\n
 * implementation delays cross the architectures
 *
 * @param i_delay Delay in ms. 0~65535
 */
void LCD1602_HAL_delay_ms(uint16_t i_delay);

/**
 * \brief Read one Byte (8bits) from flash memory
 *
 * In flash memory should be stored characters in ASCII code. Also this\n
 * function should use higher layers due to architecture depend reading from\n
 * flash memory.
 *
 * @param p_flash Pointer to flash memory
 * @return Loaded Byte from flash memory
 */
uint8_t LCD1602_HAL_read_from_flash(const char *p_flash);

#endif
