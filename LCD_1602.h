/**
 * \file
 *
 * \brief High level functions for LCD 1602
 *
 * This is library for communication with 1602 (16x2 characters) LCDs.\n
 * It is common used LCD.
 *
 * Created: a loong time ago\n
 * Modified: 27.12.2013
 *
 * \author Martin Stejskal - martin.stej@gmail.com
 *
 * \version 0.2a
 */

#ifndef _LCD_1602_library_       // If not defined then....
#define _LCD_1602_library_

//================================| Includes |=================================
/* HAL for LCD module !!! Please include correct driver for used architecture!
 * Also in included driver should be defined enumeration lcd_1602_status_t
 */
#include "LCD_1602_HAL_AVR8_SW_interface.h"

/**
 * \brief Set default settings of LCD
 *
 * Cursor settings - options: LCD_1602_cursor_blink, \n
 * LCD_1602_cursor_not_blink, LCD_1602_cursor_off
 */
#define LCD_1602_defaut_cursor  LCD_1602_cursor_blink


//================| Some checks and preprocessor calculations |================
#ifndef LCD_1602_HAL_INCLUDED
#error "No HAL library included! Please include HAL library for LCD 1602 and\
  actual architecture (AVR8, AVR32 and so on). Example:\
  #include \"LCD_1602_HAL_AVR8_SW_interface.h\""
#endif

#if (LCD_defaut_cursor != LCD_1602_cursor_off) &&\
    (LCD_defaut_cursor != LCD_1602_cursor_not_blink) &&\
    (LCD_defaut_cursor != LCD_1602_cursor_blink)
#error "LCD_1602_defaut_cursor has wrong value. Please set one of the\
  following setting: cursor_off/cursor_not_blink/cursor_blink"
#endif

//===========================| Function prototypes |===========================

/**
 * \brief Initialize LCD and clear screen
 *
 * Prepare ports and initialize LCD, so next communication with LCD will be
 * easy. Also clear display
 *
 * @return LCD_1602_OK (0) if all OK
 */
lcd_1602_status_t LCD1602_init(void);


/**
 * \brief Write whole string on LCD
 *
 * Example:\n
 *  LCD1602_write_text("test text");
 *
 * @param p_s_text Pointer to text array
 * @return LCD_1602_OK (0) if all OK
 */
lcd_1602_status_t LCD1602_write_text( char *p_s_text );


/**
 * \brief Write whole string on LCD, but insert time delay between single\n
 * characters.
 *
 * Example: LCD1602_write_text_slow( "Some text", 200);\n
 * Write string on LCD and between every symbol (character) insert time delay\n
 * 200 ms
 *
 * @param p_s_text Pointer to text array
 * @param i_delay Delay in ms
 * @return LCD_1602_OK (0) if all OK
 */
lcd_1602_status_t LCD1602_write_text_slow( char *p_s_text , uint16_t i_delay );

/**
 * \brief Write string from flash memory
 *
 * Very similar to "LCD1602_write_text", but instead of reading string\n
 * from SRAM it reads data from flash memory, which is useful on embedded\n
 * systems with small SRAM.\n
 * Example:\n
 *  LCD1602_write_text_from_flash( string_variable );\n
 * Where string variable is declared this way:\n
 *  const char string_variable[] PROGMEM = "Some text to\n be written";
 *
 * @param p_s_text_flash Pointer to text array
 * @return LCD_1602_OK (0) if all OK
 */
lcd_1602_status_t LCD1602_write_text_from_flash( const char *p_s_text_flash );

/*
 */

/**
 * \brief Write string from flash memory and between every character insert\n
 * time delay
 *
 * Again, similar to "LCD1602_write_text_slow", but instead of reading string\n
 * from SRAM it reads data from flash memory and give some time delay between\n
 * every single symbol. Delay is in ms.\n
 * Example:\n
 *  LCD1602_write_text_from_flash( string_variable, 100 );\n
 * Where string variable is declared this way:\n
 *  const char string_variable[] PROGMEM = "Some text to\n be written";\n
 * Between every symbol will be inserted 100ms delay.
 *
 * @param s_text_flash Pointer to text array
 * @param i_delay Delay in ms
 * @return LCD_1602_OK (0) if all OK
 */
lcd_1602_status_t LCD1602_write_text_from_flash_slow(
    const char * s_text_flash , uint16_t i_delay );

/**
 * \brief Write input 8 bit value as HEX number.
 *
 * Example: LCD1602_write_hex_8bit( 170 );\n
 * Write AA to LCD (170 in dec is 0xAA)
 *
 * @param i_data 8 bit hex number
 * @return LCD_1602_OK (0) if all OK
 */
lcd_1602_status_t LCD1602_write_hex_8bit( uint8_t i_data );

/**
 * \brief Write input 8 bit value as HEX number and insert between single characters
 * delay.
 *
 * Example: LCD1602_write_hex_8bit( 170 , 35);\n
 * Write AA to LCD (170 in dec is 0xAA) with 35 ms delay
 *
 * @param i_data 8 bit hex number
 * @param i_delay Delay in ms
 * @return LCD_1602_OK (0) if all OK
 */
lcd_1602_status_t LCD1602_write_hex_8bit_slow(  uint8_t i_data,
                                                uint16_t i_delay);


/**
 * \brief Write one digit (4 bit) to LCD
 *
 * Use low 4 bits and write one digit (0~F). If input number is higher than\n
 * 0x0F, then write "?" and return LCD_1602_ERROR_INVALID_PARAMETER.\n
 * Example:\n
 *  // Write to LCD symbol C\n
 *  LCD1602_write_one_digit( 0xC );
 *
 * @param i_digit Value 0~F
 * @return LCD_1602_OK (0) if all OK
 */
lcd_1602_status_t LCD1602_write_one_digit( uint8_t i_digit );


#define LCD1602_backlight_on    LCD1602_HAL_backlight_on

#define LCD1602_backlight_off   LCD1602_HAL_backlight_off

#endif

#ifndef NULL
  #define NULL (0)
#endif
