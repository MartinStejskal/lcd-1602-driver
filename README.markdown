# Driver for LCD 1602 module

## Description
 This is generic library for LCD 1602 module. Library is split into two parts:
 
 * High level driver
 * HAL driver
 
 Meanwhile high level driver is common for all platform, HAL is hardware
 depend. All HAL drivers should contain same functions, but implementation
 may be different.

## Files
 * LCD_1602.* - generic high level drivers for module. It need include HAL
 * LCD_1602_HAL_* - HAL driver

## Usage
 In LCD_1602.h, section "Included libraries" include correct HAL driver. For
 example:
  if you use AVR8, then type: #include "LCD_1602_HAL_AVR8_SW_interface.h"
 When there is not HAL driver for your favorite, then try write one ;)

## Get source code!
 `git clone https://MartinStejskal@bitbucket.org/MartinStejskal/lcd-1602-driver.git`

## Notes
 * HAL driver should contain only basic libraries
