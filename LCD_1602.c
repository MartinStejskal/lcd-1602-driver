/**
 * \file
 *
 * \brief High level functions for LCD 1602
 *
 * This is library for communication with 1602 (16x2 characters) LCDs.\n
 * It is common used LCD.
 *
 * Created: a loong time ago\n
 * Modified: 27.12.2013
 *
 * \author Martin Stejskal - martin.stej@gmail.com
 *
 * \version 0.2a
 */

#include "LCD_1602.h" // Defined pins, function prototypes


/*---------------------------------------------------------------------------*/

lcd_1602_status_t LCD1602_init(void)
{
  // OK, first initialize HAL layer (prepare display)
  return LCD_1602_HAL_init(LCD_1602_defaut_cursor);
}



/*---------------------------------------------------------------------------*/

lcd_1602_status_t LCD1602_write_text( char *p_s_text )
{
  // For store status
  lcd_1602_status_t e_status = LCD_1602_OK;

  while( (*p_s_text) != NULL )
    {
      /* Test for "\n" character (new line). If found, set address on LCD to
       * 0x40 -> first symbol on second line on LCD
       */
      if ( (*p_s_text) == '\n' )
        {
          // Try set address -> save status
          // Set LCD cursor to second line
          e_status = LCD1602_HAL_set_address( 0x40 );

          // Test status - if error -> return error code
          if(e_status != LCD_1602_OK)
          {
            return e_status;
          }

          // And ignore symbol \n -> move to next symbol
          p_s_text++;
        }

      // Try write one symbol to LCD
      e_status = LCD1602_HAL_write_char( *p_s_text);

      // Test status - if error -> return error code
      if(e_status != LCD_1602_OK)
      {
        return e_status;
      }

      // Move to next symbol/character
      p_s_text++;
    }

  return e_status;
}

/*---------------------------------------------------------------------------*/

lcd_1602_status_t LCD1602_write_text_slow( char *p_s_text , uint16_t i_delay )
{
  // For store status
  lcd_1602_status_t e_status = LCD_1602_OK;

  while( (*p_s_text) != NULL )
    {
      /* Test for "\n" character (new line). If found, set address on LCD to
       * 0x40 -> first symbol on second line on LCD
       */
      if ( (*p_s_text) == '\n' )
        {
          // Set LCD cursor to second line
          e_status = LCD1602_HAL_set_address( 0x40 );

          // Test status - if error -> return error code
          if(e_status != LCD_1602_OK)
          {
            return e_status;
          }

          // And ignore symbol \n -> move to next symbol
          p_s_text++;
        }
      // Write one symbol to LCD
      e_status = LCD1602_HAL_write_char( *p_s_text);

      // Test status - if error -> return error code
      if(e_status != LCD_1602_OK)
      {
        return e_status;
      }

      // Move to next symbol/character
      p_s_text++;

      // Insert time delay
      LCD1602_HAL_delay_ms( i_delay );
    }

  return e_status;
}

/*---------------------------------------------------------------------------*/

lcd_1602_status_t LCD1602_write_text_from_flash( const char *p_s_text_flash )
{
  // For store status
  lcd_1602_status_t e_status = LCD_1602_OK;

  while ( LCD1602_HAL_read_from_flash(p_s_text_flash) != NULL )
  {
    if ( LCD1602_HAL_read_from_flash(p_s_text_flash) == '\n' )
    {
      e_status = LCD1602_HAL_set_address( 0x40 );
      // Test status - if error -> return error code
      if(e_status != LCD_1602_OK)
      {
        return e_status;
      }

      p_s_text_flash++;
    }
    e_status = LCD1602_HAL_write_char(LCD1602_HAL_read_from_flash(
                                                            p_s_text_flash) );

    // Test status - if error -> return error code
    if(e_status != LCD_1602_OK)
    {
      return e_status;
    }

    p_s_text_flash++;
  }

  return e_status;
}

/*---------------------------------------------------------------------------*/

lcd_1602_status_t LCD1602_write_text_from_flash_slow(
    const char * p_s_text_flash , uint16_t i_delay )
{
  // For store status
  lcd_1602_status_t e_status = LCD_1602_OK;

  while ( LCD1602_HAL_read_from_flash(p_s_text_flash) != NULL )
  {
    if ( LCD1602_HAL_read_from_flash(p_s_text_flash) == '\n' )
    {
      e_status = LCD1602_HAL_set_address( 0x40 );

      // Test status - if error -> return error code
      if(e_status != LCD_1602_OK)
      {
        return e_status;
      }

      p_s_text_flash++;
    }
    e_status = LCD1602_HAL_write_char( LCD1602_HAL_read_from_flash(
                                                             p_s_text_flash) );
    // Test status - if error -> return error code
    if(e_status != LCD_1602_OK)
    {
      return e_status;
    }

    p_s_text_flash++;
    LCD1602_HAL_delay_ms( i_delay );
  }

  return e_status;
}

/*---------------------------------------------------------------------------*/

lcd_1602_status_t LCD1602_write_hex_8bit( uint8_t i_data )
{
  // For store status
  lcd_1602_status_t e_status = LCD_1602_OK;

  // First is needed to split 8 bit value to 2x4 bits

  // First should be written MSB to LCD
  uint8_t byte_part = (i_data & 0b11110000)>>4; /* Copy 4 MSB bits and rotate
                                                 * them by 4 to the right
                                                 */
  // Write 4 bits as HEX value (0~F)
  e_status = LCD1602_write_one_digit( byte_part );

  // Test status - if error -> return error code
  if(e_status != LCD_1602_OK)
  {
    return e_status;
  }

  // Now LSB
  byte_part = (i_data & 0b00001111);    // No shift needed, just copy LSB
  e_status = LCD1602_write_one_digit( byte_part ); // And write to LCD

  return e_status;
}

/*---------------------------------------------------------------------------*/

lcd_1602_status_t LCD1602_write_hex_8bit_slow(  uint8_t i_data,
                                                uint16_t i_delay)
{
  // For store status
  lcd_1602_status_t e_status = LCD_1602_OK;

  // First is needed to split 8 bit value to 2x4 bits

  // First should be written MSB to LCD
  uint8_t byte_part = (i_data & 0b11110000)>>4; /* Copy 4 MSB bits and rotate
                                                 * them by 4 to the right
                                                 */
  // Write 4 bits as HEX value (0~F)
  e_status = LCD1602_write_one_digit( byte_part );
  // Test status - if error -> return error code
  if(e_status != LCD_1602_OK)
  {
    return e_status;
  }

  LCD1602_HAL_delay_ms( i_delay );

  // Now LSB
  byte_part = (i_data & 0b00001111);    // No shift needed, just copy LSB
  e_status = LCD1602_write_one_digit( byte_part ); // And write to LCD

  LCD1602_HAL_delay_ms( i_delay );

  return e_status;
}


/*---------------------------------------------------------------------------*/

lcd_1602_status_t LCD1602_write_one_digit( uint8_t i_digit )
{
  // For store status
  lcd_1602_status_t e_status = LCD_1602_OK;

  // Test for input value. If bigger than 0xF -> can not display -> show "?"
  if ( i_digit > 0xF )
    {
      LCD1602_HAL_write_char( '?');
      // Well, this should not happen. Input variable is wrong, so return error
      return LCD_1602_ERROR_INVALID_PARAMETER;
    }

  // Test for range 0~9
  if ( i_digit < 10 )
    {
      // Give write_char ASCII number
      e_status = LCD1602_HAL_write_char( 48 + i_digit );

      // Test status - if error -> return error code
      if(e_status != LCD_1602_OK)
      {
        return e_status;
      }
    }
  else  // A~F
    {
      // Again, just give ASCII number
      // 65 is A symbol -> A is 10 -> 65-10
      e_status = LCD1602_HAL_write_char( 55 + i_digit );

      // Test status - if error -> return error code
      if(e_status != LCD_1602_OK)
      {
        return e_status;
      }
    }

  return e_status;
}

/*---------------------------------------------------------------------------*/
