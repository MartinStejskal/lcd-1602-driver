/**
 * \file
 *
 * \brief Hardware Abstraction Layer for AVR8 and LCD 1602 module.\n
 * Use generic pins (no hardware module needed)
 *
 * HAL driver for 4bit communication with module LCD 1602.\n
 * Contains basic low level functions which depend on user architecture and\n
 * hardware. Thanks to this layer is possible use common higher level driver\n
 * and apply it thru many architectures and devices. Also this allow easy\n
 * updates independent to higher layer (new features and so on).
 *
 * Created:  25.12.2013\n
 * Modified: 26.12.2013
 *
 * \version 0.1a
 * \author Martin Stejskal
 */

#include "LCD_1602_HAL_AVR8_SW_interface.h"

//=================================| Macros |==================================
/**
 * \name Set pin as output
 *
 * Macros, that easily make one line command for setting pin as output.\n
 * Very effective. Based on bit_operations library.
 *
 * @{
 */
#define LCD_1602_io_set_dir_out_simple(X,pin)\
  DDR##X = DDR##X | (1<< pin)
#define LCD_1602_io_set_dir_out(X,pin)\
  LCD_1602_io_set_dir_out_simple(X,pin)
/**
 * @}
 */
/**
 * \name Set output pin to logic 1
 *
 * Very effective macro which set output pin to logic 1. Based on\n
 * bit_operations library.
 *
 * @{
 */
#define LCD_1602_io_set_1_simple(X,pin)\
  PORT##X = PORT##X | (1 << pin )
#define LCD_1602_io_set_1(X,pin)\
  LCD_1602_io_set_1_simple(X,pin)
/**
 * @}
 */
/**
 * \name Set output pin to logic 0
 *
 * Very effective macro which set output pin to logic 0. Based on\n
 * bit_operations library.
 *
 * @{
 */
#define LCD_1602_io_set_0_simple(X,pin)\
  PORT##X = PORT##X & (~(1 << pin))
#define LCD_1602_io_set_0(X,pin)\
  LCD_1602_io_set_0_simple(X,pin)
/**
 * @}
 */


/**
 * \name Macros for setting pins output
 *
 * Following lines should not be changed (not needed) unless You really know\n
 * what are you doing
 *
 * @{
 */
///\brief Set E to 0
#define LCD_1602_E_0         LCD_1602_io_set_0(LCD_1602_E_PORT,LCD_1602_E_PIN)
///\brief Set E to 1
#define LCD_1602_E_1         LCD_1602_io_set_1(LCD_1602_E_PORT,LCD_1602_E_PIN)

///\brief Set RS to 0
#define LCD_1602_RS_0        LCD_1602_io_set_0(LCD_1602_RS_PORT,LCD_1602_RS_PIN)
///\brief Set RS to 1
#define LCD_1602_RS_1        LCD_1602_io_set_1(LCD_1602_RS_PORT,LCD_1602_RS_PIN)

///\brief Set D4 to 0
#define LCD_1602_D4_0        LCD_1602_io_set_0(LCD_1602_D4_PORT,LCD_1602_D4_PIN)
///\brief Set D4 to 1
#define LCD_1602_D4_1        LCD_1602_io_set_1(LCD_1602_D4_PORT,LCD_1602_D4_PIN)

///\brief Set D5 to 0
#define LCD_1602_D5_0        LCD_1602_io_set_0(LCD_1602_D5_PORT,LCD_1602_D5_PIN)
///\brief Set D5 to 1
#define LCD_1602_D5_1        LCD_1602_io_set_1(LCD_1602_D5_PORT,LCD_1602_D5_PIN)

///\brief Set D6 to 0
#define LCD_1602_D6_0        LCD_1602_io_set_0(LCD_1602_D6_PORT,LCD_1602_D6_PIN)
///\brief Set D6 to 1
#define LCD_1602_D6_1        LCD_1602_io_set_1(LCD_1602_D6_PORT,LCD_1602_D6_PIN)

///\brief Set D7 to 0
#define LCD_1602_D7_0        LCD_1602_io_set_0(LCD_1602_D7_PORT,LCD_1602_D7_PIN)
///\brief Set D7 to 1
#define LCD_1602_D7_1        LCD_1602_io_set_1(LCD_1602_D7_PORT,LCD_1602_D7_PIN)

/**
 * @}
 */

//==================| Function prototypes not seen by user |===================
/**
 * \brief Prepare pins, witch will be used for communication with LCD
 *
 * Just set all pins as outputs
 */
void LCD1602_set_IO(void);

/**
 * \brief Send 8 data bits to LCD.
 *
 * Do no set RS signal, so programmer must set it before this function is\n
 * called.
 */
void LCD1602_send_8bits( uint8_t i_data );

/**
 * \brief Low-level function. Just generate one enable pulse for LCD
 */
void LCD1602_Signal_enable(void);

//================================| Functions |================================

inline lcd_1602_status_t LCD_1602_HAL_init(uint8_t i_default_cursor)
{
  // Check input variable
  if( (i_default_cursor != LCD_1602_cursor_off) &&
      (i_default_cursor != LCD_1602_cursor_not_blink) &&
      (i_default_cursor != LCD_1602_cursor_blink))
  {
    // If not valid default cursor value -> reurn error
    return LCD_1602_ERROR_INVALID_PARAMETER;
  }

  // OK, input variables seems to be all right. Let's initialize module...

  // Set I/O to outputs
  LCD1602_set_IO();

  _delay_ms(10);        /* Can not initialize LCD "right now", must wait at
                         * least 10ms
                         */

  // Set 4 bit data transmit
  LCD_1602_RS_0;
  LCD_1602_D7_0;
  LCD_1602_D6_0;
  LCD_1602_D5_1;
  LCD_1602_D4_0;     // Data length: 4 bits
  LCD1602_Signal_enable();      // And confirm data on bus


  // Set function set
  LCD_1602_RS_0;
  LCD_1602_D7_0;
  LCD_1602_D6_0;
  LCD_1602_D5_1;
  LCD_1602_D4_0;     // Data length: 4 bits
  LCD1602_Signal_enable();      // And confirm data on bus

  LCD_1602_RS_0;
  LCD_1602_D7_1;     // N - 1 means 2 lines ; 0 - 1 line
  LCD_1602_D6_1;     // F - font size        ; (0 "not working")
  LCD_1602_D5_0;
  LCD_1602_D4_0;
  LCD1602_Signal_enable();      // Confirm data on bus


  // Display and cursor settings
  LCD_1602_RS_0;
  LCD_1602_D7_0;
  LCD_1602_D6_0;
  LCD_1602_D5_0;
  LCD_1602_D4_0;
  LCD1602_Signal_enable();

  LCD_1602_RS_0;
  LCD_1602_D7_1;
  LCD_1602_D6_1;     // Display ON - 1

  // Cursor settings
#if LCD_defaut_cursor == cursor_off
  LCD_1602_D5_0;     // Cursor OFF
#else
  LCD_1602_D5_1;     // Cursor ON - 1
#endif

#if LCD_defaut_cursor == cursor_blink
  LCD_1602_D4_1;     // Blinking cursor ON - 1
#else
  LCD_1602_D4_0;     // Blinking cursor OFF - 0
#endif

  LCD1602_Signal_enable();


  // Entry mode set
  LCD_1602_RS_0;
  LCD_1602_D7_0;
  LCD_1602_D6_0;
  LCD_1602_D5_0;
  LCD_1602_D4_0;
  LCD1602_Signal_enable();

  LCD_1602_RS_0;
  LCD_1602_D7_0;
  LCD_1602_D6_1;
  LCD_1602_D5_1;     // Increment cursor - 1
  LCD_1602_D4_0;     // No shift - 0
  LCD1602_Signal_enable();

  // And clear LCD - return status ; Also set RS to 1 (default)
  return LCD1602_HAL_Clear_display();
}

/*---------------------------------------------------------------------------*/

lcd_1602_status_t LCD1602_HAL_Clear_display(void)
{
 LCD_1602_RS_0;
 LCD_1602_D7_0;
 LCD_1602_D6_0;
 LCD_1602_D5_0;
 LCD_1602_D4_0;
 LCD1602_Signal_enable();

 LCD_1602_RS_0;
 LCD_1602_D7_0;
 LCD_1602_D6_0;
 LCD_1602_D5_0;
 LCD_1602_D4_1;
 LCD1602_Signal_enable();

 LCD_1602_RS_1;     // LCD RS set to "1"

 return LCD_1602_OK;
}

/*---------------------------------------------------------------------------*/

lcd_1602_status_t LCD1602_HAL_set_address( uint8_t i_addr )
{
  // Test for validity input variable
  if ( (i_addr > 0x67 ) || ( (i_addr > 0x27) && (i_addr < 0x40) )  )
    {
      return LCD_1602_ERROR_INVALID_PARAMETER; // Return non-zero value
    }

  /* Else variable seems to be OK -> MSB must be 1 -> add 1 to i_addr to MSB
   * position
   */
  i_addr = i_addr | (1<<7);

  LCD_1602_RS_0;

  LCD1602_send_8bits( i_addr ); // And send data to bus

  LCD_1602_RS_1;     // LCD RS set to "1"

  return LCD_1602_OK;
}

/*---------------------------------------------------------------------------*/

lcd_1602_status_t LCD1602_HAL_write_char( uint8_t i_char )
{
  LCD1602_send_8bits( i_char );     // And send raw data

  return LCD_1602_OK;
}

/*---------------------------------------------------------------------------*/

lcd_1602_status_t LCD1602_HAL_backlight_on(void)
{
#if LCD_1602_backlight_support == 1
  // Set backlight pin to high
  LCD_1602_io_set_1(LCD_1602_LIGHT_PORT, LCD_1602_LIGHT_PIN);

  return LCD_1602_OK;
#else
  return LCD_1602_ERROR;
#endif
}

/*---------------------------------------------------------------------------*/

lcd_1602_status_t LCD1602_HAL_backlight_off(void)
{
#if LCD_1602_backlight_support == 1
  // Set backlight pin to low
  LCD_1602_io_set_0(LCD_1602_LIGHT_PORT, LCD_1602_LIGHT_PIN);

  return LCD_1602_OK;
#else
  return LCD_1602_ERROR;
#endif
}


/*---------------------------------------------------------------------------*/

void LCD1602_HAL_delay_ms(uint16_t i_delay)
{
  uint16_t i;
  for(i=0 ; i<i_delay ; i++ )
  {
    // 900 us, because for cycle take some cycles too
    _delay_us(900);
  }
}

/*---------------------------------------------------------------------------*/

inline uint8_t LCD1602_HAL_read_from_flash(const char *p_flash)
{
  return pgm_read_byte(p_flash);
}

/*---------------------------------------------------------------------------*/

inline void LCD1602_set_IO(void)
{
  // Set output values to 0 and then set pins as outputs
  LCD_1602_io_set_0(LCD_1602_E_PORT,  LCD_1602_E_PIN);
  LCD_1602_io_set_0(LCD_1602_RS_PORT, LCD_1602_RS_PIN);
  LCD_1602_io_set_0(LCD_1602_D4_PORT, LCD_1602_D4_PIN);
  LCD_1602_io_set_0(LCD_1602_D5_PORT, LCD_1602_D5_PIN);
  LCD_1602_io_set_0(LCD_1602_D6_PORT, LCD_1602_D6_PIN);
  LCD_1602_io_set_0(LCD_1602_D7_PORT, LCD_1602_D7_PIN);


  // First set Enable pin
  LCD_1602_io_set_dir_out(LCD_1602_E_PORT, LCD_1602_E_PIN);

  // Set RS pin
  LCD_1602_io_set_dir_out(LCD_1602_RS_PORT, LCD_1602_RS_PIN);

  // Set D4 to D7
  LCD_1602_io_set_dir_out(LCD_1602_D4_PORT, LCD_1602_D4_PIN);
  LCD_1602_io_set_dir_out(LCD_1602_D5_PORT, LCD_1602_D5_PIN);
  LCD_1602_io_set_dir_out(LCD_1602_D6_PORT, LCD_1602_D6_PIN);
  LCD_1602_io_set_dir_out(LCD_1602_D7_PORT, LCD_1602_D7_PIN);

  // And light pin if support is enabled
#if LCD_1602_backlight_support == 1
  LCD_1602_io_set_dir_out(LCD_1602_LIGHT_PORT, LCD_1602_LIGHT_PIN);
#endif
}

/*---------------------------------------------------------------------------*/


inline void LCD1602_send_8bits( uint8_t i_data )
{
  // First we need send high 4 bits - set high bits
  if ( ((1 << 7) & i_data ) > 0 )       /* Little complicated. So, first is
                                         * created "mask" 0b10000000. Then is
                                         * done AND operation on bit level with
                                         * variable "i_data". So, if on MSB was
                                         * logical 1, then logical 1 is copy to
                                         * condition. Else is copy 0 of MSB.
                                         * Anyway, if value is non-zero, then
                                         * is needed to set pin to 1. Else set
                                         * to 0.
                                         */
    {   // Is MSB is 1 -> set 1
      LCD_1602_D7_1;
    }
  else  // set 0
    {
      LCD_1602_D7_0;
    }

  // x << y - shift "x" by "y" bits to left
  if ( ((1 << 6) & i_data ) > 0 )
    {   // Is bit is is 1 -> set 1
      LCD_1602_D6_1;
    }
  else  // set 0
    {
      LCD_1602_D6_0;
    }

  if ( ((1 << 5) & i_data ) > 0 )
    {   // Is bit is is 1 -> set 1
      LCD_1602_D5_1;
    }
  else  // set 0
    {
      LCD_1602_D5_0;
    }

  if ( ((1 << 4) & i_data ) > 0 )
    {   // Is bit is is 1 -> set 1
      LCD_1602_D4_1;
    }
  else  // set 0
    {
      LCD_1602_D4_0;
    }

  LCD1602_Signal_enable();      // Send enable -> LCD recieve high 4 bits


  // Now send low 4 bits

  if ( ((1 << 3) & i_data ) > 0 )
    {   // Is bit is is 1 -> set 1
      LCD_1602_D7_1;
    }
  else  // set 0
    {
      LCD_1602_D7_0;
    }

  if ( ((1 << 2) & i_data ) > 0 )
    {   // Is bit is is 1 -> set 1
      LCD_1602_D6_1;
    }
  else  // set 0
    {
      LCD_1602_D6_0;
    }

  if ( ((1 << 1) & i_data ) > 0 )
    {   // Is bit is is 1 -> set 1
      LCD_1602_D5_1;
    }
  else  // set 0
    {
      LCD_1602_D5_0;
    }

  if ( ((1 << 0) & i_data ) > 0 )
    {   // Is bit is is 1 -> set 1
      LCD_1602_D4_1;
    }
  else  // set 0
    {
      LCD_1602_D4_0;
    }

  LCD1602_Signal_enable();      // Again, confirm data on bus
}

/*---------------------------------------------------------------------------*/

inline void LCD1602_Signal_enable(void)
{
  //_delay_ms(1);
  LCD_1602_E_1;       // Set E to 1
  _delay_ms(1);
  LCD_1602_E_0;       // Set E to 0
  _delay_ms(1);
}

